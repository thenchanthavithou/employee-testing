package com.example.employeeservice.repository;

import com.example.employeeservice.model.Employee;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class EmployeeRepositoryTests {
    private final EmployeeRepository employeeRepository;
    private Employee employee;

    @Test
    void name() {
    }

    @BeforeEach
    public void setUp() {
        employee = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@example.com")
                .build();
        Employee savedEmployee = employeeRepository.save(employee);
    }

    @Autowired
    public EmployeeRepositoryTests(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    //junit test for save employee operation
    @DisplayName("junit test for save employee operation")
    @Test
    public void givenEmployeeObject_whenSave_thenReturnedSaveEmployee() {
        //given - precondition or set up
        //when - action or behaviour that we are going to test
        Employee savedEmployee = employeeRepository.save(employee);
        //then - verify the output
        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getId()).isGreaterThan(0);
    }

    //JUnit test for get all employees operations
    @DisplayName("JUnit test for get all employees operations")
    @Test
    public void givenEmployees_whenFindAll_thenReturnedEmployees() {
        //given - precondition or set up
        Employee employee1 = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@example.com")
                .build();

        Employee employee2 = Employee.builder()
                .firstName("John")
                .lastName("Cena")
                .email("cena@example.com")
                .build();

        Employee employee3 = Employee.builder()
                .firstName("larry")
                .lastName("little")
                .email("larry@example.com")
                .build();

        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);


        //when - action or behaviour that we are going to test
        List<Employee> employees = employeeRepository.findAll();
        //then - verify the output
        assertThat(employees).isNotNull();
        assertThat(employees.size()).isEqualTo(5);
    }

    //JUnit test for get employee by id operation
    @DisplayName("JUnit test for get employee by id operation")
    @Test
    public void givenEmployee_whenFindById_thenReturnedEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);
        //when - action or behaviour that we are going to test
        Employee foundEmployee = employeeRepository.findById(employee.getId()).get();
        //then - verify the output
        assertThat(foundEmployee).isNotNull();
        assertThat(foundEmployee.getId()).isEqualTo(employee.getId());
    }

    //JUnit test
    @Test
    public void givenEmployee_whenFindByEmail_thenReturnedEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);
        //when - action or behaviour that we are going to test
        Employee foundEmployee = employeeRepository.findEmployeeByEmail(employee.getEmail()).get();
        //then - verify the output
        assertThat(foundEmployee).isNotNull();
        assertThat(foundEmployee.getEmail()).isEqualTo(employee.getEmail());
    }

    //JUnit test
    @Test
    public void givenEmployee_whenUpdateEmployee_thenReturnedUpdateEmployee() {
        employeeRepository.save(employee);
        //when - action or behaviour that we are going to test
        Employee foundEmployee = employeeRepository.findById(employee.getId()).get();
        foundEmployee.setEmail("janes@example.com");
        foundEmployee.setFirstName("janes");
        foundEmployee.setLastName("jones");
        Employee updatedEmployee = employeeRepository.save(foundEmployee);
        //then - verify the output
        assertThat(updatedEmployee.getEmail()).isEqualTo("janes@example.com");
        assertThat(updatedEmployee.getFirstName()).isEqualTo("janes");
    }

    //JUnit test
    @Test
    public void givenEmployee_whenDelete_thenRemoveEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);
        //when - action or behaviour that we are going to test
        employeeRepository.deleteById(employee.getId());
        Optional<Employee> employeeOptional = employeeRepository.findById(employee.getId());
        //then - verify the output
        assertThat(employeeOptional).isEmpty();
    }

    //    JUnit test for custom query using JPQL with index
    @DisplayName("JUnit test for custom query using JPQL with index")
    @Test
    public void givenFirstNameAndLastName_whenFindByJPQL_thenReturnedEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);

        String firstName = "John";
        String lastName = "Doe";
        //when - action or behaviour that we are going to test
        Employee foundEmployee = employeeRepository.findByJPQL(firstName, lastName);
        //then - verify the output
        assertThat(foundEmployee).isNotNull();
    }

    //JUnit test for custom query using JPQL params
    @Test
    public void givenFirstNameAndLastName_whenFindJPQLNamedParams_thenReturnedEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);

        String firstName = "John";
        String lastName = "Doe";
        //when - action or behaviour that we are going to test
        Employee foundEmployee = employeeRepository.findByJPQLNamedParams(firstName, lastName);
        //then - verify the output
        assertThat(foundEmployee).isNotNull();
    }
}
