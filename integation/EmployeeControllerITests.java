package com.example.employeeservice.integation;

import com.example.employeeservice.model.Employee;
import com.example.employeeservice.service.impl.EmployeeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class EmployeeControllerITests {
    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    private final EmployeeServiceImpl employeeService;

    @Test
    void name() {

    }

    @Autowired
    public EmployeeControllerITests(MockMvc mockMvc, ObjectMapper objectMapper, EmployeeServiceImpl employeeService) {
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
        this.employeeService = employeeService;
    }

    @BeforeEach
    public void setUp() {
        employeeService.deleteAllEmployee();
    }

    @Test
    public void givenEmployee_whenCreateEmployee_thenReturnedEmployee() throws Exception {
        //given - precondition or set up
        Employee employee = Employee
                .builder()
                .id(1L)
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(post("/v1/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employee)));
        //then - verify the output
        response.andDo(print()).andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(employee.getLastName())))
                .andExpect(jsonPath("$.email", is(employee.getEmail())));
    }

    @Test
    public void givenEmployeeList_whenGetAllEmployees_thenReturnedEmployeeList() throws Exception {
        //given - employee list
        Employee jane = Employee.builder()
                .firstName("Janes")
                .lastName("Jones")
                .email("janes@gmail.com")
                .build();
        Employee john = Employee.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();
        employeeService.saveEntity(jane);
        employeeService.saveEntity(john);
        //when - get all employees
        ResultActions response = mockMvc.perform(get("/v1/employees"));
        //then - returned all employees
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()", is(2)));
    }

    @Test
    public void givenEmployeeId_whenGetEmployeeById_thenReturnedEmployee() throws Exception {
        //given - jane as a employee object
        Employee jane = Employee.builder()
                .firstName("Janes")
                .lastName("Jones")
                .email("janes@gmail.com")
                .build();
        employeeService.saveEntity(jane);
        //when - get the response by id path url
        ResultActions response = mockMvc.perform(get("/v1/employees/{id}", jane.getId()));
        //then - returned employee as expected
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.firstName", is("Janes")))
                .andExpect(jsonPath("$.lastName", is("Jones")))
                .andExpect(jsonPath("$.email", is("janes@gmail.com")));
    }


    @Test
    public void givenEmployeeId_whenGetEmployeeByIdNotExisting_thenReturnedNothing() throws Exception {
        //given - jane as a employee object
        Employee jane = Employee.builder()
                .firstName("Janes")
                .lastName("Jones")
                .email("janes@gmail.com")
                .build();
        employeeService.saveEntity(jane);
        //when - get the response by id path url
        ResultActions response = mockMvc.perform(get("/v1/employees/{id}", 10));
        //then - returned employee as expected
        response.andExpect(status().isNotFound())
                .andDo(print());
    }

    @DisplayName("Integration for testing update employee")
    @Test
    public void givenEmployeeIdAndNewEmployee_whenUpdateEmployee_thenReturnedNewEmployee() throws Exception {
        //given employee and new employee
        Employee jane = Employee.builder()
                .firstName("Janes")
                .lastName("Jones")
                .email("janes@gmail.com")
                .build();
        employeeService.saveEntity(jane);

        Employee newEmployee =
                Employee.builder()
                        .firstName("Vithou")
                        .lastName("Then")
                        .email("vithou@gmail.com")
                        .build();
        //when update employee
        ResultActions response = mockMvc.perform(put("/v1/employees/{id}", jane.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newEmployee)));
        //then response new employee
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.firstName", is(newEmployee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(newEmployee.getLastName())))
                .andExpect(jsonPath("$.email", is(newEmployee.getEmail())));
    }

    @Test
    public void givenEmployeeId_whenDeleteEmployeeById_thenReturnedNothing() throws Exception {
        // - given - setup or precondition
        Employee jane = Employee.builder()
                .firstName("Janes")
                .lastName("Jones")
                .email("janes@gmail.com")
                .build();
        employeeService.saveEntity(jane);
        // - when implement what we are going to test
        ResultActions response = mockMvc.perform(delete("/v1/employees/{id}", jane.getId()));
        // - then response what we've implements
        response.andExpect(status().isOk())
                .andDo(print());
    }
}
