package com.example.employeeservice.controller;

import com.example.employeeservice.model.Employee;
import com.example.employeeservice.service.impl.EmployeeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.CoreMatchers.*;

import org.junit.jupiter.api.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmployeeServiceImpl employeeService;

    @Autowired
    private ObjectMapper mapper;

    //JUnit test
    @Test
    public void givenEmployee_whenCreateEmployee_thenReturnedEmployee() throws Exception {
        //given - precondition or set up
        Employee employee = Employee
                .builder()
                .id(1L)
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();
        given(employeeService.saveEntity(ArgumentMatchers.any(Employee.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(post("/v1/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(employee)));
        //then - verify the output
        response.andDo(print()).andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(employee.getLastName())))
                .andExpect(jsonPath("$.email", is(employee.getEmail())));
    }

    //JUnit test for getting all employees
//    @Test
//    public void givenEmployeesList_whenGetAllEmployees_thenReturnedEmployeesList() throws Exception {
//        //given - precondition or set up
//        List<Employee> listOfEmployees = new ArrayList<>();
//        listOfEmployees.add(Employee.builder()
//                .id(1L)
//                .firstName("John")
//                .lastName("Doe")
//                .email("john@gmail.com")
//                .build());
//        listOfEmployees.add(Employee.builder()
//                .id(2L)
//                .firstName("Janes")
//                .lastName("Jones")
//                .email("janes@gmail.com")
//                .build());
//        given(employeeService.getAllEmployees()).willReturn(listOfEmployees);
//        //when - action or behaviour that we are going to test
//        ResultActions response = mockMvc.perform(get("/v1/employees"));
//        //then - verify the output
//        response.andExpect(status().isOk())
//                .andDo(print())
//                .andExpect(jsonPath("$.size()", is(listOfEmployees.size())));
//    }

    //Positive scenario - valid employee id
    @Test
    public void givenEmployeeId_whenGetEmployeeById_thenReturnedEmployee() throws Exception {
        //given - precondition or set up
        Long employeeId = 1L;
        Employee employee = Employee
                .builder()
                .id(1L)
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();
        given(employeeService.getEmployeeById(employeeId))
                .willReturn(Optional.of(employee));
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(get("/v1/employees/{id}", employeeId));
        //then - verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.firstName", is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(employee.getLastName())))
                .andExpect(jsonPath("$.email", is(employee.getEmail())));

    }

    //Negative scenario - valid employee id
    @Test
    public void givenEmployeeId_whenGetEmployeeById_thenReturnedEmpty() throws Exception {
        //given - precondition or set up
        Long employeeId = 1L;
        Employee employee = Employee
                .builder()
                .id(1L)
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();
        given(employeeService.getEmployeeById(employeeId))
                .willReturn(Optional.empty());
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(get("/v1/employees/{id}", employeeId));
        //then - verify the output
        response.andExpect(status().isNotFound())
                .andDo(print());
    }

    //JUnit test for updating rest api
    @Test
    public void givenEmployee_whenUpdateEmployee_thenReturnedUpdatedEmployee() throws Exception {
        //given - precondition or set up
        Long empId = 1L;
        Employee employee = Employee
                .builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();

        Employee updatedEmployee = Employee
                .builder()
                .firstName("Vithou")
                .lastName("Then")
                .email("vithou@gmail.com")
                .build();

        given(employeeService.getEmployeeById(empId))
                .willReturn(Optional.of(employee));
        given(employeeService.updateEmployee(ArgumentMatchers.any(Employee.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(put("/v1/employees/{id}", empId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updatedEmployee)));
        //then - verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.firstName", is(updatedEmployee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(updatedEmployee.getLastName())))
                .andExpect(jsonPath("$.email", is(updatedEmployee.getEmail())));
    }

    //JUnit test for updating rest api negative scenario
    @Test
    public void givenEmployee_whenUpdateEmployee_thenReturnedNothing() throws Exception {
        //given - precondition or set up
        Long empId = 1L;
        Employee employee = Employee
                .builder()
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();

        Employee updatedEmployee = Employee
                .builder()
                .firstName("Vithou")
                .lastName("Then")
                .email("vithou@gmail.com")
                .build();

        given(employeeService.getEmployeeById(empId))
                .willReturn(Optional.empty());
        given(employeeService.updateEmployee(ArgumentMatchers.any(Employee.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(put("/v1/employees/{id}", empId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updatedEmployee)));
        //then - verify the output
        response
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    //JUnit test
    @Test
    public void givenEmployeeId_whenDeleteEmployee_thenReturnedNothing() throws Exception {
        //given - precondition or set up
        Long id = 1L;
        Employee employee = Employee
                .builder()
                .id(1L)
                .firstName("John")
                .lastName("Doe")
                .email("john@gmail.com")
                .build();
        willDoNothing().given(employeeService).deleteEmployee(id);
        //when - action or behaviour that we are going to test
        ResultActions response = mockMvc.perform(delete("/v1/employees/{id}", id));
        //then - verify the output
        response.andExpect(status().isOk())
                .andDo(print());
    }
}
