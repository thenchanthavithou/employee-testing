package com.example.employeeservice.service;

import com.example.employeeservice.exception.ResourceNotFoundException;
import com.example.employeeservice.model.Employee;
import com.example.employeeservice.repository.EmployeeRepository;
import com.example.employeeservice.service.impl.EmployeeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTests {
    @Mock
    private EmployeeRepository employeeRepository;
    @InjectMocks
    private EmployeeServiceImpl employeeService;

    private Employee employee;

    @Test
    void name() {
    }

    @BeforeEach
    public void setUp() {
        employee = Employee.builder()
                .id(1L)
                .firstName("Vithou")
                .lastName("Then")
                .email("vithou@gmail.com")
                .build();
    }

    //    JUnit test for saveEmployee method
    @DisplayName("JUnit test for saveEmployee method")
    @Test
    public void givenEmployee_whenSaveEmployee_thenReturnedEmployeeDTO() {
        //given - precondition or set up
        given(employeeRepository.findEmployeeByEmail(employee.getEmail()))
                .willReturn(Optional.empty());
        given(employeeRepository.save(employee))
                .willReturn(employee);

        //when - action or behaviour that we are going to test

        Employee saveEmployee = employeeService.saveEntity(employee);

        //then - verify the output
        assertThat(saveEmployee).isNotNull();
    }

    @DisplayName("JUnit test for saveEmployee method which throw exception")
    @Test
    public void givenExistingEmail_whenSaveEmployee_thenThrowsException() {
        //given - precondition or set up
        given(employeeRepository.findEmployeeByEmail(employee.getEmail()))
                .willReturn(Optional.of(employee));

        //when - action or behaviour that we are going to test
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            employeeService.saveEntity(employee);
        });

        //then - verify the output
        verify(employeeRepository, never()).save(any(Employee.class));
    }

    //JUnit test for get all employees
//    @DisplayName("JUnit test for get all employees")
//    @Test
//    public void givenEmployees_whenGetAllEmployees_thenReturnedEmployeeList() {
//        Employee employee1 = Employee.builder()
//                .id(2L)
//                .firstName("Tony")
//                .lastName("Stark")
//                .email("tony@gmail.com")
//                .build();
//
//        //given - precondition or set up
//        given(employeeRepository.findAll())
//                .willReturn(List.of(employee, employee1));
//        //when - action or behaviour that we are going to test
//        List<Employee> employees = employeeService.getAllEmployees();
//        //then - verify the output
//        assertThat(employees).isNotNull();
//        assertThat(employees).hasSize(2);
//    }


    //JUnit test for get all employees
//    @DisplayName("JUnit test for get all employees for negative scenarios")
//    @Test
//    public void givenEmptyEmployees_whenGetAllEmployees_thenReturnedEmptyEmployees() {
//        Employee employee1 = Employee.builder()
//                .id(2L)
//                .firstName("Tony")
//                .lastName("Stark")
//                .email("tony@gmail.com")
//                .build();
//
//        //given - precondition or set up
//        given(employeeRepository.findAll())
//                .willReturn(Collections.emptyList());
//        //when - action or behaviour that we are going to test
//        List<Employee> employees = employeeService.getAllEmployees();
//        //then - verify the output
//        assertThat(employees).isEmpty();
//        assertThat(employees).hasSize(0);
//    }

    //JUnit test for getting the employee by id
    @Test
    public void givenEmployeeId_whenGetEmployeeById_thenReturnedEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);
        given(employeeRepository.findById(1L))
                .willReturn(Optional.of(employee));
        //when - action or behaviour that we are going to test
        Employee foundEmployee = employeeService.getEmployeeById(employee.getId()).get();
        //then - verify the output
        assertThat(foundEmployee).isNotNull();
        //then - verify the output
    }

    //JUnit test
    @Test
    public void givenEmployee_whenUpdateEmployee_thenReturnedEmployee() {
        //given - precondition or set up
        employeeRepository.save(employee);
        employee.setFirstName("Janes");
        employee.setLastName("Jones");
        employee.setEmail("Janes@example.com");
        given(employeeRepository.save(employee))
                .willReturn(employee);
        //when - action or behaviour that we are going to test
        Employee updatedEmployee = employeeService.updateEmployee(employee);
        //then - verify the output
        assertThat(updatedEmployee.getFirstName()).isEqualTo("Janes");
    }

    //JUnit test for deleting an employee
    @DisplayName("JUnit test for deleting an employee")
    @Test
    public void givenEmployeeId_whenDeleteEmployee_thenReturnedNothing() {
        //given - precondition or set up
        willDoNothing().given(employeeRepository).deleteById(2L);
        //when - action or behaviour that we are going to test
        employeeService.deleteEmployee(2L);
        //then - verify the output
        verify(employeeRepository, times(1)).deleteById(2L);
    }
}
